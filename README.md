
# React native advanced template

## Dummy for mobile application with connected packages

- [React](https://reactjs.org)
- [React-native](https://facebook.github.io/react-native)
- [Redux](https://redux.js.org)
- [Axios](https://www.npmjs.com/package/axios)
- [Lodash](https://lodash.com/docs)
- [Moment](https://momentjs.com)
- [Prop-types](https://www.npmjs.com/package/prop-types)
- [React-redux](https://github.com/reactjs/react-redux)
- [Redux-form](https://redux-form.com)
- [Redux-saga](https://redux-saga.js.org)
- [React-navigation](https://reactnavigation.org/)
- [React-native-svg](https://www.npmjs.com/package/react-native-svg)
- [React-native-svg-icon](https://www.npmjs.com/package/react-native-svg-icon)
- [React-native-device-info](https://www.npmjs.com/package/react-native-device-info)
- [React-native-hyperlink](https://www.npmjs.com/package/react-native-hyperlink)
- [React-native-root-toast](https://www.npmjs.com/package/react-native-root-toast)
- [React-native-animatable](https://www.npmjs.com/package/react-native-animatable)
- [React-native-camera](https://www.npmjs.com/package/react-native-camera)
- [React-native-vector-icons](https://www.npmjs.com/package/react-native-vector-icons)
- [React-native-gesture-handler](https://www.npmjs.com/package/react-native-gesture-handler)

It`s also highly recommended to use react-native-debugger as a debugger. You can also check out the React Native documentation on debugging for suggestions.

- [React-native-debugger](https://github.com/jhen0409/react-native-debugger)

#### Fork and run

Fork this repository:
```
> git clone https://logvinovMaksim@bitbucket.org/logvinovMaksim/rn-template.git
```

Run the following command in a Command Prompt or shell:
```
> npm install -g react-native-cli
```

install dependencies:
```
> yarn 
or
> npm install
```

Then link dependencies to android and ios:
```
> react-native link
```

Then start the development process:
```
> npm run start
```

To start development process on android:
```
> npm run android
```

To start development process on ios:
```
> npm run ios
```

#### Setup custom configuration

Flow of **create-react-native-app** [Getting started](https://facebook.github.io/react-native/docs/getting-started) is fully supported. 

#### Android

Open "android" folder of your application in Android Studio. Synchronize with gradlew. After installing/deleting dependencies which covers android files you should resynchronise your application with gradlew.

Make sure that you set correct configurations to your Android Studio. To see list of configurations visit [Getting started](https://facebook.github.io/react-native/docs/getting-started)/

NOTE: at the moment of writing this template ( June 2019 ) migrating android part of project to androidx leads to crashing of application. This problem related to libraries that wasn`t updated at this moment:

- [React-native-camera](https://www.npmjs.com/package/react-native-camera)
- [React-native-gesture-handler](https://www.npmjs.com/package/react-native-gesture-handler)
