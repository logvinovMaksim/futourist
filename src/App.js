
// outsource dependencies
import React from 'react';
import { Provider } from 'react-redux';
import { useScreens } from 'react-native-screens';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

// local dependencies
import NavigationService from './services/navigation';
import store from './store';
import PublicScreens from './screens/public-screens/routes';
import PrivateScreens from './screens/private-screens/routes';

useScreens(true);

const AppScreens = createAppContainer(
    createSwitchNavigator({
        Public: PublicScreens,
        Private: PrivateScreens,
    })
);

class App extends React.Component {
    // componentDidMount = () => NavigationService.initialize();

    // componentWillUnmount = () => NavigationService.clear();

    render () {
        return (
            <Provider store={store}>
                <AppScreens ref={NavigationService.setupNavigator} />
            </Provider>
        );
    }
}

export default App;
