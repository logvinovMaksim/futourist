
// define rule to create actions pack for async actions
export const ASYNC_SUFFIX = {
    REQUEST: 'REQUEST',
    START: 'START',
    SUCCESS: 'SUCCESS',
    FINISH: 'FINISH',
    ERROR: 'ERROR',
    CLEAR: 'CLEAR',
};

/**
 * create object with types to build async actions package
 *
 * @param {String} action - name of action to create many action
 * @param {Object} [expand={}] - provide ability to add non standard actions
 * @return {Object}
 * @public
 */
export function createTypes (action, expand = {}) {
    for (const name in ASYNC_SUFFIX) {
        expand[name] = `${action}_${ASYNC_SUFFIX[name]}`;
    }
    return expand;
}

export const APP = (prefix => ({
    INIT: createTypes(`${prefix}INIT`),
}))('@app/');

