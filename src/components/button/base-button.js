
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

// local dependencies
import Text from '../text';
import { APP_COLORS } from '../../constants/app.theme';

const styles = StyleSheet.create({
    baseButton: {
        minWidth: 64,
        borderStyle: 'solid',
        borderRadius: 4,
        marginBottom: 16,
    },
    content: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    icon: {
        margin: 16,
    },
    iconLeft: {
        left: 0,
        marginRight: -8,
        // position: 'absolute'
    },
    iconRight: {
        right: 0,
        marginLeft: -8,
    },
    label: {
        textAlign: 'center',
        margin: 16,
    },
    disabled: {
        opacity: 0.5,
    },
});

class BaseButton extends PureComponent {
    get styles () {
        const { buttonStyles, disabled, labelStyles } = this.props;
        const button = disabled
            ? StyleSheet.flatten([styles.baseButton, buttonStyles, styles.disabled])
            : StyleSheet.flatten([styles.baseButton, buttonStyles]);
        const label = labelStyles ? StyleSheet.flatten([styles.label, labelStyles]) : styles.label;
        return { button, label };
    }

    get defineIcon () {
        const { icon, iconRight, label, iconStyles, children } = this.props;
        if (children) {
            return children;
        }
        const { name = 'question', size = 20, color = APP_COLORS.WHITE } = icon;
        // If no label present then icon must be centered
        let iconContainer = StyleSheet.flatten([styles.icon, iconStyles]);
        // If label present then define icon position
        if (label) {
            iconContainer = iconRight
                ? StyleSheet.flatten([iconContainer, styles.iconRight, iconStyles])
                : StyleSheet.flatten([iconContainer, styles.iconLeft, iconStyles]);
        }
        return (
            <View style={iconContainer}>
                <Icon name={name} size={size} color={color} />
            </View>
        );
    }

    render () {
        const { label, onPress, disabled, icon, labelColor, labelVariant, children } = this.props;
        return (
            <View style={this.styles.button}>
                <TouchableOpacity onPress={!disabled ? onPress : null}>
                    <View style={styles.content}>
                        {(Boolean(icon) || Boolean(children)) && this.defineIcon}
                        {Boolean(label) && (
                            <Text variant={labelVariant} style={this.styles.label} color={labelColor}>
                                {label}
                            </Text>
                        )}
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

BaseButton.propTypes = {
    children: PropTypes.node,
    label: PropTypes.string,
    labelColor: PropTypes.string,
    labelVariant: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    icon: PropTypes.object,
    iconRight: PropTypes.bool,
    iconStyles: PropTypes.object,
    onPress: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    buttonStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
    labelStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
};

BaseButton.defaultProps = {
    children: null,
    labelVariant: 'h4',
    label: null,
    labelColor: APP_COLORS.FONTS_COLOR,
    icon: null,
    iconRight: null,
    buttonStyles: null,
    iconStyles: null,
    labelStyles: null,
    disabled: false,
};

export default BaseButton;
