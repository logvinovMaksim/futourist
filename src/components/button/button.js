
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';

// local dependencies
import BaseButton from './base-button';
import { APP_COLORS } from '../../constants/app.theme';

// configurations
export const BUTTON_TYPE = {
    MAIN: 'MAIN',
    SECONDARY: 'SECONDARY',
    SOCIAL: 'SOCIAL',
    CATEGORY: 'CATEGORY',
    LOW: 'LOW',
};

export const SHADOW_STYLE = {
    shadowOffset: {
        width: 0,
        height: 4,
    },
    shadowOpacity: 0.24,
    shadowRadius: 8,
    elevation: 5,
};

const BORDER = {
    borderRadius: 45,
    borderWidth: 2,
};

const styles = StyleSheet.create({
    main: {
        backgroundColor: APP_COLORS.MAIN,
        borderColor: APP_COLORS.MAIN,
        shadowColor: APP_COLORS.THEME_COLOR,
        width: '100%',
        ...SHADOW_STYLE,
        ...BORDER,
    },
    secondary: {
        backgroundColor: 'transparent',
        borderColor: APP_COLORS.MAIN,
        shadowColor: APP_COLORS.THEME_COLOR,
        width: '100%',
        ...BORDER,
    },
    category: {
        backgroundColor: 'transparent',
        borderColor: APP_COLORS.TURQUOISE,
        paddingHorizontal: 8,
        ...BORDER,
    },
    activeCategory: {
        backgroundColor: APP_COLORS.TURQUOISE,
        borderColor: APP_COLORS.TURQUOISE,
        paddingHorizontal: 8,
        ...BORDER,
    },
    social: {
        shadowColor: APP_COLORS.THEME_COLOR,
        ...SHADOW_STYLE,
        width: 140,
    },
    socialIcon: {
        marginRight: 2,
    }
});

class Button extends PureComponent {
    constructor () {
        super();
        this.state = {
            active: false,
        };
    }

    get category () {
        const { active } = this.state;
        const unactiveCategory = {
            style: styles.category,
            labelColor: APP_COLORS.TURQUOISE,
            iconColor: APP_COLORS.TURQUOISE,
        };
        const activeCategory = {
            style: styles.activeCategory,
            labelColor: APP_COLORS.WHITE,
            iconColor: APP_COLORS.WHITE,
        };
        return active ? activeCategory: unactiveCategory;
    }

    get social () {
        return { style: [styles.social, this.props.buttonStyles], iconStyle: styles.socialIcon };
    }

    handleCategoryActivity = () => {
        const { active } = this.state;
        this.setState({ active: !active });
        // prevent app crash if onPress were not defined
        this.props.onPress && this.props.onPress();
    };

    render () {
        const { type, label, icon, onPress, buttonStyles, ...attr } = this.props;
        switch (type) {
            case BUTTON_TYPE.SOCIAL:
                return (
                    <BaseButton
                        {...attr}
                        labelVariant="common"
                        buttonStyles={this.social.style}
                        iconStyles={this.social.iconStyle}
                        label={label}
                        onPress={onPress}
                        icon={icon}
                    />
                );
            case BUTTON_TYPE.CATEGORY:
                return (
                    <BaseButton
                        {...attr}
                        label={label}
                        labelVariant="common"
                        buttonStyles={[this.category.style, buttonStyles]}
                        onPress={this.handleCategoryActivity}
                        labelColor={this.category.labelColor}
                        icon={{ name: icon.name, size: icon.size, color: this.category.iconColor }}
                    />
                );
            case BUTTON_TYPE.SECONDARY:
                return (
                    <BaseButton
                        {...attr}
                        label={label}
                        buttonStyles={styles.secondary}
                        labelColor={APP_COLORS.MAIN}
                        onPress={onPress}
                    />
                );
            case BUTTON_TYPE.LOW:
                return (
                    <BaseButton {...attr} label={label} labelColor={APP_COLORS.WHITE} onPress={onPress} />
                );
            default:
                return (
                    <BaseButton
                        {...attr}
                        label={label}
                        buttonStyles={styles.main}
                        labelColor={APP_COLORS.WHITE}
                        onPress={onPress}
                    />
                );
        }
    }
}

Button.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    // labelColor: PropTypes.string,
    icon: PropTypes.object,
    iconRight: PropTypes.bool,
    iconStyles: PropTypes.object,
    onPress: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    buttonStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
    labelStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
};

Button.defaultProps = {
    type: null,
    label: null,
    // labelColor: null,
    icon: null,
    iconRight: null,
    buttonStyles: null,
    iconStyles: null,
    labelStyles: null,
    disabled: false,
};

export default Button;
