
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

// local dependencies
import { APP_COLORS, APP_OFFSETS } from '../../constants/app.theme';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
    },
    logo: {
        width: 32,
        height: 32,
        marginLeft: APP_OFFSETS.LEFT,
    },
});

class AppHeader extends PureComponent {
    get icon () {
        const { arrow } = this.props;
        return arrow ? 'ios-arrow-back' : 'ios-close';
    }

    close = () => this.props.navigation.goBack(null);

    render () {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.close}>
                    <Icon style={styles.logo} name={this.icon} size={30} color={APP_COLORS.BLACK} />
                </TouchableOpacity>
            </View>
        );
    }
}

AppHeader.propTypes = {
    navigation: PropTypes.object.isRequired,
    arrow: PropTypes.bool,
};

AppHeader.defaultProps = {
    arrow: false,
};

export default AppHeader;
