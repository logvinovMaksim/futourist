
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

// local dependencies
import { APP_COLORS, APP_OFFSETS, COMMON_OFFSET } from '../../constants/app.theme';
import { SCREEN_WIDTH } from '../../constants/device';

const ICON_SIZE = 30;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        width: '100%',
        borderBottomWidth: 2,
        borderBottomColor: '#748787',
        flexDirection: 'row',
        marginBottom: APP_OFFSETS.BOTTOM,
    },
    input: {
        height: 40,
        width: '100%',
    },
});

class BaseInput extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
            visibility: false,
        };
    }

    get visibilityIconColor () {
        const { visibility } = this.state;
        return visibility ? APP_COLORS.THEME_DISABLED_COLOR : APP_COLORS.BLACK;
    }

    get input () {
        const { icon, password, search } = this.props;
        // set place for icon, common offset because of offsets in screen component
        const inputWidth = search
            ? SCREEN_WIDTH - COMMON_OFFSET * 6 - ICON_SIZE
            : SCREEN_WIDTH - COMMON_OFFSET * 4 - ICON_SIZE;
        return (icon || password || search) ? StyleSheet.flatten([styles.input, { width: inputWidth }]) : styles.input;
    }

    get icon () {
        const { icon, password, search } = this.props;

        if (password) { return (<Icon name="low-vision" size={ICON_SIZE} color={this.visibilityIconColor} />); }

        if (search) { return (<Icon name="search" size={20} color={APP_COLORS.BLACK} />); }

        if (typeof icon === 'object') {
            const { name = 'question', size = ICON_SIZE, color = APP_COLORS.BLACK } = icon;
            return (<Icon name={name} size={size} color={color} />);
        }

        return (<Icon name="question" size={ICON_SIZE} color={APP_COLORS.BLACK} />);

    }

    get style () {
        const { inputStyles } = this.props;
        return StyleSheet.flatten([styles.container, inputStyles]);
    }

    handleVisibility = () => {
        const { visibility } = this.state;
        this.setState({ visibility: !visibility });
    };

    handleOnIconPress = () => {
        const { password, onIconPress } = this.props;
        password
            ? this.handleVisibility()
            : (typeof onIconPress === 'function') && onIconPress();
    };

    render () {
        const { visibility } = this.state;
        const { placeholder, icon, password, search, ...attr } = this.props;
        console.log('BaseInput', this.props);
        return (
            <View style={this.style}>
                <TextInput
                    {...attr}
                    style={this.input}
                    placeholder={placeholder}
                    secureTextEntry={visibility}
                />
                {(Boolean(icon) || password || search) && (
                    <TouchableOpacity
                        onPress={this.handleOnIconPress}
                        style={{ justifyContent: 'center', alignItems: 'center' }}
                    >
                        {this.icon}
                    </TouchableOpacity>
                )}
            </View>
        );
    }
}

BaseInput.propTypes = {
    placeholder: PropTypes.string,
    icon: PropTypes.object,
    password: PropTypes.bool,
    search: PropTypes.bool,
    onIconPress: PropTypes.func,
    inputStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

BaseInput.defaultProps = {
    placeholder: null,
    icon: null,
    password: false,
    search: false,
    onIconPress: null,
    inputStyles: null,
};

export default BaseInput;
