
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';

// local dependencies
import BaseInput from './base-input';
import { APP_COLORS } from '../../constants/app.theme';

// configurations
export const INPUT_TYPE = {
    PASSWORD: 'PASSWORD',
    SEARCH: 'SEARCH',
};

const styles = StyleSheet.create({
    search: {
        borderWidth: 2,
        borderColor: APP_COLORS.WHITE,
        borderRadius: 45,
        backgroundColor: APP_COLORS.WHITE,
        paddingLeft: 16,
        // shadow
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.24,
        shadowRadius: 8,
        elevation: 5,
    },
});

class Input extends PureComponent {
    constructor () {
        super();
        this.state = {
            active: false,
        };
    }

    render () {
        const { type, ...attr } = this.props;
        switch (type) {
            case INPUT_TYPE.PASSWORD:
                return (
                    <BaseInput
                        {...attr}
                        password
                    />
                );
            case INPUT_TYPE.SEARCH:
                return (
                    <BaseInput
                        {...attr}
                        inputStyles={styles.search}
                        search
                    />
                );
            default:
                return (
                    <BaseInput
                        {...attr}
                    />
                );
        }
    }
}

Input.propTypes = {
    // type: PropTypes.string,
    // label: PropTypes.string,
    // // labelColor: PropTypes.string,
    // icon: PropTypes.object,
    // iconRight: PropTypes.bool,
    // iconStyles: PropTypes.object,
    // onPress: PropTypes.func.isRequired,
    // disabled: PropTypes.bool,
    // buttonStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
    // labelStyles: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
};

Input.defaultProps = {
    // type: null,
    // label: null,
    // // labelColor: null,
    // icon: null,
    // iconRight: null,
    // buttonStyles: null,
    // iconStyles: null,
    // labelStyles: null,
    // disabled: false,
};

export default Input;
