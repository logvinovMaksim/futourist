
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { ImageBackground, View, ActivityIndicator, StyleSheet } from 'react-native';

// local dependencies
import { APP_COLORS } from '../constants/app.theme';

// configuration
export const TYPE = {
    SPINNER: 'SPINNER',
    BOX: 'BOX',
};

const styles = StyleSheet.create({
    boxPreloader: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    defPreloaderView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    defPreloaderImage: {
        width: '100%',
        height: '100%',
    }
});

class Preloader extends PureComponent {
    render () {
        const { type, active, children, ...attr } = this.props;
        // NOTE do nothing
        if (!active) { return children; }
        // NOTE show preloader
        switch (type) {
            case TYPE.BOX: return <BoxPreloader {...attr} />;
            case TYPE.SPINNER: return <ActivityIndicator {...attr} />;
            default: return <DefPreloader />;
        }
    }
}

Preloader.propTypes = {
    active: PropTypes.bool,
    type: PropTypes.oneOf(Object.keys(TYPE).map(key => TYPE[key])),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.element,
        PropTypes.node
    ]),
};

Preloader.defaultProps = {
    active: false,
    children: null,
    type: null,
};

export default Preloader;

class DefPreloader extends PureComponent {
    render () {
        return (
            <ImageBackground
                style={styles.defPreloaderView}
                imageStyle={styles.defPreloaderImage}
                source={require('../../assets/car.jpg')}
            >
                <ActivityIndicator size="large" color={APP_COLORS.WHITE} />
            </ImageBackground>
        );
    }
}

class BoxPreloader extends PureComponent {
    get style () {
        // NOTE setup default style
        let allStyles = [styles.boxPreloader];
        // NOTE setup custom style
        if (this.props.style) {
            allStyles = allStyles.concat(this.props.style);
        }
        return StyleSheet.flatten(allStyles);
    }

    render () {
        const { size } = this.props;
        return (
            <View style={this.style}>
                <ActivityIndicator size={size} />
            </View>
        );
    }
}
BoxPreloader.propTypes = {
    size: PropTypes.oneOf(['small', 'large']),
    style: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.array
    ]),
};
BoxPreloader.defaultProps = {
    style: null,
    size: 'large',
};

// shortcut
export const BoxHolder = props => <Preloader type={TYPE.BOX} {...props} />;
export const Spinner = props => <Preloader type={TYPE.SPINNER} {...props} />;
