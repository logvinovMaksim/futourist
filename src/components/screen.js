
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import _ from 'lodash';
import { withNavigationFocus, NavigationEvents } from 'react-navigation';
import { View, StyleSheet, BackHandler, ImageBackground, Keyboard } from 'react-native';

// local dependencies
import { BoxHolder } from './preloader';
import { SCREEN_STYLES } from '../constants/app.theme';


const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        display: 'flex',
    },
    screen: {
        ...SCREEN_STYLES.COMMON,
    },
    modalScreen: {
        ...SCREEN_STYLES.MODAL,
    },
});
// Avoid multiply screens back
const limitedGoBack = _.throttle(nav => nav.pop(), 200, { leading: true, trailing: false });

class Screen extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
            screenFocused: false,
        };
    }

    componentDidMount () {
        const { init, keyboard } = this.props;
        // BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        if (typeof keyboard === 'function') {
            this.keyboardListener = Keyboard.addListener('keyboardDidShow', this.handleKeyboard);
        }
        if (typeof init === 'function') {
            init();
        }
    }

    componentWillUnmount () {
        const { clear, keyboard } = this.props;
        // BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        if (typeof keyboard === 'function') {
            this.keyboardListener.remove();
        }
        if (typeof clear === 'function') {
            clear();
        }
    }

    handleKeyboard = () => {
        const { keyboard } = this.props;
        if (typeof keyboard === 'function') { keyboard(); }
        // NOTE stop event propagation
        return true;
    };

    handleScreenFocus = () => this.setState({ screenFocused: true });

    handleScreenDidBlur = () => this.setState({ screenFocused: false });

    handleBackPress = () => {
        const { navigation, onBackPress, allowDefaultBack, isFocused } = this.props;
        const { screenFocused, initialized } = this.state;
        if (!isFocused || !initialized) { return; }
        if (typeof onBackPress === 'function') { onBackPress(); }
        if (allowDefaultBack && screenFocused) { limitedGoBack(navigation); }
        // NOTE stop event propagation
        return true;
    };

    render () {
        const { children, style, initialized = true, source } = this.props;
        const ScreenContainer = source ? ImageBackground : View;
        return (
            <ScreenContainer style={styles.screenContainer} source={source}>
                <BoxHolder active={!initialized}>
                    <View style={[styles.screen, style]}>
                        { children }
                    </View>
                </BoxHolder>
                <NavigationEvents
                    onWillFocus={this.handleScreenFocus}
                    onDidFocus={this.handleScreenFocus}
                    onDidBlur={this.handleScreenDidBlur}
                />
            </ScreenContainer>
        );
    }
}

Screen.propTypes = {
    source: PropTypes.number,
    init: PropTypes.func,
    clear: PropTypes.func,
    style: PropTypes.object,
    children: PropTypes.node,
    isFocused: PropTypes.bool,
    onBackPress: PropTypes.func,
    allowDefaultBack: PropTypes.bool,
    // initialized: PropTypes.bool.isRequired,
    initialized: PropTypes.bool,
    navigation: PropTypes.object.isRequired,
    keyboard: PropTypes.func,
};

Screen.defaultProps = {
    source: null,
    init: null,
    clear: null,
    style: null,
    children: null,
    isFocused: false,
    onBackPress: null,
    allowDefaultBack: false,
    keyboard: null,
    initialized: true
};


export default withNavigationFocus(Screen);
