
// outsource dependencies
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { StyleSheet, Text as UIText } from 'react-native';

// local dependencies
import { APP_COLORS } from '../constants/app.theme';
import * as textStyles from '../constants/text.style';

// configure
const allowedAlign = ['auto', 'left', 'right', 'center', 'justify'];

class Text extends PureComponent {
    get textStyle () {
        let allStyles = [];
        const { variant, textAlign, color, style } = this.props;
        // NOTE setup base style based on text variant
        allStyles.push(textStyles[variant] || textStyles.common);
        // NOTE setup align
        if (allowedAlign.indexOf(textAlign) > -1) {
            allStyles.push({ textAlign });
        }
        // NOTE setup custom stylesheet
        if (style) {
            allStyles = allStyles.concat(style);
        }
        // NOTE setup color
        color && allStyles.push({ color });
        return StyleSheet.flatten(allStyles);
    }

    render = () => (
        <UIText style={this.textStyle}>
            { this.props.children }
        </UIText>
    );
}
Text.propTypes = {
    color: PropTypes.string,
    children: PropTypes.node,
    textAlign: PropTypes.oneOf(allowedAlign),
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    variant: PropTypes.oneOf(Object.keys(textStyles).map(key => key)),
};
Text.defaultProps = {
    style: null,
    children: null,
    textAlign: null,
    variant: 'common',
    color: APP_COLORS.FONTS_COLOR,
};

export default Text;
