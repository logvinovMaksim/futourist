
// take configuration from environment
const config = {
    DEBUG: Boolean(process.env.DEBUG),
    apiPath: typeof process.env.API_PATH === 'string' ? process.env.API_PATH : '/api',
    serviceUrl: typeof process.env.SERVICE_URL === 'string' ? process.env.SERVICE_URL : 'http://82.117.249.182:9192',
};

console.info(
    '%c CONFIG ',
    'background: #EC1B24; color: #000; font-weight: bolder; font-size: 30px;'
    , '\n full ENV:', process.env
    , '\n NODE_ENV:', process.env.NODE_ENV
    , '\n REACT_APP_ENV:', process.env.REACT_APP_ENV
    , '\n config:', config
);

export default config;
export { config };
