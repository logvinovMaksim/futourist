
// Need this constant to pass LOCAL images dynamically in Image and ImageBackground component
// because of problems with require() util

// local images
import START from '../../assets/start-screen.jpg';
import CAR from '../../assets/car.jpg';

const LOCAL_IMAGES = {
    START,
    CAR,
};

export default LOCAL_IMAGES;
