
// outsource dependencies

// local dependencies
import { config } from './config';

export const ACTIVITY = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('activity');

export const REVIEW = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('review');

export const REVIEW_PARAMETERS = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('review-parameters');

export const HOME = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('home');

export const LIST_HOLDER = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('list-holder');

export const COLLECTIONS = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('collections');

export const OUTFITS = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('outfits');
export const PLACES = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('places');

export const REVIEW_WATCH = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('review-watch');

export const SINGLE_COLLECTION = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('single-collection');

export const SINGLE_OUTFIT = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('single-outfit');

export const SINGLE_PLACE = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('single-place');

export const PROFILE = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('profile');

export const EDIT_PROFILE = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('edit-profile');

export const SETTINGS = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('settings');

export const TERMS_OF_SERVICE = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('terms-of-service');

export const SEARCH = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('search');

export const LOG_IN = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('log-in');

export const SIGN_UP = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('sign-up');

export const START = (base => ({
    ROUTE: base,
    LINK: linkTo.bind({ routeName: base })
}))('start');

/**
 * @description
 interface NavigationNavigateActionPayload {
    routeName: string;
    params?: NavigationParams;
    // The action to run inside the sub-router
    action?: NavigationNavigateAction;
    key?: string;
 }
 * @example SETTINGS.LINK({ some: 'urlParam' query: {some: 'queryParam'}})
 * @param { Object } options
 * @returns { Object }
 * @function linkTo
 * @private
 */
function linkTo (options) {
    const { params } = options || {};
    config.DEBUG && console.info(
        '%c linkTo => ( options ) '
        , 'color: #0747a6; font-weigth: bolder; font-size: 12px;'
        , '\n options:', options
        , '\n routeName:', this.routeName
    );
    return {
        routeName: this.routeName,
        params: params || this.params,
    };
}
