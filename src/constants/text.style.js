
// outsource dependencies
// import { StyleSheet } from 'react-native';

// local dependencies
import { APP_COLORS, DEFAULT_FONT_STYLES } from './app.theme';

/* --------------------
        TEXT
   -------------------- */
export const common = {
    ...DEFAULT_FONT_STYLES,
    fontSize: 14,
    letterSpacing: 0.44,
};

export const caption = {
    ...common,
    fontSize: 12,
};

export const small = {
    ...common,
    fontSize: 10,
};

export const bold = {
    ...common,
    fontWeight: '600',
};

export const errorMessage = {
    ...small,
    color: APP_COLORS.NEGATIVE_RED,
};

/* --------------------
        TITLE
   -------------------- */
export const screenTitle = {
    ...DEFAULT_FONT_STYLES,
    fontWeight: '500',
    fontSize: 20,
    letterSpacing: 0,
};

export const h1 = {
    ...bold,
    fontSize: 24,
};

export const h2 = {
    ...common,
    fontSize: 22,
    fontWeight: '500',
    letterSpacing: 0.15,
};

export const h3 = {
    ...DEFAULT_FONT_STYLES,
    fontSize: 20,
    fontWeight: '500',
    letterSpacing: 0.15,
};

export const h4 = {
    ...DEFAULT_FONT_STYLES,
    fontSize: 17,
    fontWeight: '500',
    letterSpacing: 0.15,
};

export const h5 = {
    ...common,
    fontWeight: '500',
};

export const h6 = {
    ...caption,
    fontWeight: '500',
};
