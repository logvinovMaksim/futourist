
// outsource dependencies
import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

// local dependencies
import app from './app';
import privateReducers from '../screens/private-screens/reducers';
import publicReducers from '../screens/public-screens/reducers';

// connect
const rootReducer = combineReducers({
    app,
    form,
    ...publicReducers,
    ...privateReducers,
});

export default rootReducer;
