
// outsource dependencies
import { take, put, takeEvery } from 'redux-saga/effects';

// local dependencies
import { APP } from '../actions/types';

function* appInit () {
    try {
        yield put({ type: APP.INIT.FINISH });
    } catch (e) {

    }
    yield put({ type: APP.INIT.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* appWatcher () {
    yield takeEvery(APP.INIT.REQUEST, appInit);
}
