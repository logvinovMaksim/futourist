
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import { APP_OFFSETS } from '../../../constants/app.theme';
import TYPE from './types';
import Screen from '../../../components/screen';
import Text from '../../../components/text';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    getInfoBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class ActivityScreen extends PureComponent {
    activityRequest = () => this.props.activityRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text textAlign="center" variant="h1">Activity screen</Text>
                    {/*<Text variant="subtitle">You currently at Activity Screen</Text>*/}
                    {/*<View style={styles.getInfoBtn}>*/}
                    {/*<Button title="Get Info" onPress={this.activityRequest} />*/}
                    {/*</View>*/}
                </View>
            </Screen>
        );
    }
}

ActivityScreen.displayName = 'ActivityScreen';

ActivityScreen.propTypes = {
    activityRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.activity }),
    dispatch => ({
        activityRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(ActivityScreen);
