
// outsource dependencies
import React from 'react';
import { createStackNavigator } from 'react-navigation';

// local dependencies
import ActivityScreen from './index';
import AppHeader from '../../../components/header/app-header';

const ActivityStack = createStackNavigator(
    {
        Activity: {
            screen: ActivityScreen,
            navigationOptions: ({ navigation }) => ({
                title: 'Activity',
                headerLeft: <AppHeader navigation={navigation} arrow />,
            }),
        }
    },
    {
        headerLayoutPreset: 'center',
        defaultNavigationOptions: {
            headerLayoutPreset: 'center',
        }
    }
);

export default ActivityStack;
