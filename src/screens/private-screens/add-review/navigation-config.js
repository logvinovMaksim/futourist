
// outsource dependencies
import React from 'react';
import { createStackNavigator } from 'react-navigation';

// local dependencies
import AppHeader from '../../../components/header/app-header';
import * as ROUTES from '../../../constants/routes';
import ReviewParametersScreen from './review-parameters';
import ReviewScreen from './review';

const ReviewStack = createStackNavigator(
    {
        [ROUTES.REVIEW.ROUTE]: {
            screen: ReviewScreen,
        },
        [ROUTES.REVIEW_PARAMETERS.ROUTE]: {
            screen: ReviewParametersScreen,
        }
    },
    {
        headerLayoutPreset: 'center',
        defaultNavigationOptions: ({ navigation }) => ({
            headerLayoutPreset: 'center',
            title: 'Add review',
            headerLeft: <AppHeader navigation={navigation} arrow />,
        })
    }
);

export default ReviewStack;
