
// local dependencies
import review from './review/reducer';
import reviewParameters from './review-parameters/reducer';

/**
 * connect all review private reducers
 *
 */
export default {
    review,
    reviewParameters,
};
