
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import { APP_OFFSETS } from '../../../../constants/app.theme';
import { REVIEW_PARAMETERS } from '../types';
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    getInfoBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class ReviewParametersScreen extends PureComponent {
    reviewParametersRequest = () => this.props.reviewParametersRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Review Parameters Screen</Text>
                    <View style={styles.getInfoBtn}>
                        <Button title="Get Info" onPress={this.reviewParametersRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

ReviewParametersScreen.displayName = 'ReviewParametersScreen';

ReviewParametersScreen.propTypes = {
    activityRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.reviewParameters }),
    dispatch => ({
        reviewParametersRequest: () => dispatch({ type: REVIEW_PARAMETERS.REQUEST }),
    })
)(ReviewParametersScreen);
