
// local dependencies
import { REVIEW_PARAMETERS } from '../types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case REVIEW_PARAMETERS.START:
            state = { expectAnswer: true };
            break;
        case REVIEW_PARAMETERS.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
