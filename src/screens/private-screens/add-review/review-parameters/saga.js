
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { REVIEW_PARAMETERS } from '../types';

function* reviewParametersFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: REVIEW_PARAMETERS.SUCCESS });
    } catch (error) {
        yield put({ type: REVIEW_PARAMETERS.ERROR, errorMessage: error.message });
    }
    yield put({ type: REVIEW_PARAMETERS.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(REVIEW_PARAMETERS.REQUEST, reviewParametersFlow);
}
