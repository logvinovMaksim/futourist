
// local dependencies
import { REVIEW } from '../types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case REVIEW.START:
            state = { expectAnswer: true };
            break;
        case REVIEW.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
