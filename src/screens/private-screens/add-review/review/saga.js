
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { REVIEW } from '../types';

function* reviewFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: REVIEW.SUCCESS });
    } catch (error) {
        yield put({ type: REVIEW.ERROR, errorMessage: error.message });
    }
    yield put({ type: REVIEW.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(REVIEW.REQUEST, reviewFlow);
}
