
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import review from './review/saga';
import reviewParameters from './review-parameters/saga';

/**
 * connect all profile private sagas
 *
 */
export default function* () {
    yield fork(review);
    yield fork(reviewParameters);
}
