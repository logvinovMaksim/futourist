// local dependencies

export const REVIEW = (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@review/');

export const REVIEW_PARAMETERS = (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@review-parameters/');
