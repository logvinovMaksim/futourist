
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button, ImageBackground, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';

// local dependencies
import { APP_COLORS, APP_OFFSETS } from '../../../constants/app.theme';
import { SCREEN_HEIGHT } from '../../../constants/device';
import TYPE from './types';
import Screen from '../../../components/screen';
import Text from '../../../components/text';
import Input, { INPUT_TYPE } from '../../../components/input/input';

const styles = StyleSheet.create({
    content: {
        paddingHorizontal: 16,
        height: SCREEN_HEIGHT / 1.5,
        justifyContent: 'flex-end',
    },
    getInfoBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class HomeScreen extends PureComponent {
    componentDidMount () {
        console.info('firebase.auth()', firebase.auth());
    }

    homeRequest = () => this.props.homeRequest();

    goToSearchScreen = () => this.props.navigation.navigate('Search');

    render () {
        const { infoList } = this.props;
        return (
            <Screen initialized>
                <ScrollView style={{ height: SCREEN_HEIGHT, margin: -16 }}>
                    <ImageBackground
                        source={require('../../../../assets/home.jpg')}
                        style={styles.content}
                    >
                        <Text
                            textAlign="center"
                            variant="h1"
                            color={APP_COLORS.WHITE}
                            style={{ marginBottom: 16 }}
                        >
                            Welcome to charming Slovenia
                        </Text>
                        <Input
                            ref={ref => this._search = ref}
                            type={INPUT_TYPE.SEARCH}
                            placeholder="What you`re exploring?"
                        />
                    </ImageBackground>
                    {/*<Button title="START" onPress={() => this.props.navigation.navigate('Public')} />*/}
                    {/*<View style={styles.getInfoBtn}>*/}
                    {/*<Button title="Get Info" onPress={this.homeRequest} />*/}
                    {/*</View>*/}
                    {/*<View style={styles.getInfoBtn}>*/}
                    {/*<Button title="Search Screen" onPress={this.goToSearchScreen} />*/}
                    {/*</View>*/}
                </ScrollView>
            </Screen>
        );
    }
}

HomeScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    homeRequest: PropTypes.func.isRequired,
    infoList: PropTypes.array.isRequired,
};

export default connect(
    state => ({ ...state.home }),
    dispatch => ({
        homeRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(HomeScreen);
