
// outsource dependencies
import { createStackNavigator } from 'react-navigation';

// local dependencies
import HomeScreen from './index';

const HomeStack = createStackNavigator(
    {
        HomeScreen: {
            screen: HomeScreen,
            navigationOptions: {
                header: null,
            }
        },
    }
);

export default HomeStack;
