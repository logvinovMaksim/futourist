
// local dependencies
import TYPE from './types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
    infoList: [],
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case TYPE.START:
            state = { expectAnswer: true };
            break;
        case TYPE.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
