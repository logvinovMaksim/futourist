
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class CollectionsScreen extends PureComponent {
    collectionsRequest = () => this.props.collectionsRequest();

    goToSettingsScreen = () => this.props.navigation.navigate('SettingsScreen');

    goToSingleOutfitScreen = () => this.props.navigation.navigate('SingleOutfitScreen');

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Collections Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.collectionsRequest} />
                    </View>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Settings Screen" onPress={this.goToSettingsScreen} />
                    </View>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Single Outfit Screen" onPress={this.goToSingleOutfitScreen} />
                    </View>
                </View>
            </Screen>
        );
    }
}

CollectionsScreen.displayName = 'CollectionsScreen';

CollectionsScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    collectionsRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.collections }),
    dispatch => ({
        collectionsRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(CollectionsScreen);
