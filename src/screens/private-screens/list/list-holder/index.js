
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class ListHolderScreen extends PureComponent {
    listHolderRequest = () => this.props.listHolderRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text variant="h6">Welcome to React Native advanced template!</Text>
                    <Text variant="h5">You currently at List Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.listHolderRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

ListHolderScreen.displayName = 'ListHolderScreen';

ListHolderScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    listHolderRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.listHolder }),
    dispatch => ({
        listHolderRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(ListHolderScreen);
