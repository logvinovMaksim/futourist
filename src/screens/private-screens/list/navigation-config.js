
// outsource dependencies
import { createStackNavigator } from 'react-navigation';

// local dependencies
import * as ROUTES from '../../../constants/routes';
import SingleOutfitScreen from './single-outfit';
import SingleCollectionScreen from './single-collection';
import SinglePlaceScreen from './single-place';
import PlacesScreen from './places';
import ListHolderScreen from './list-holder';
import ReviewWatchScreen from './review-watch';
import OutfitsScreen from './outfits';
import CollectionsScreen from './collections';

const ListStack = createStackNavigator(
    {
        [ROUTES.COLLECTIONS.ROUTE]: {
            screen: CollectionsScreen,
        },
        [ROUTES.LIST_HOLDER.ROUTE]: {
            screen: ListHolderScreen,
        },
        [ROUTES.OUTFITS.ROUTE]: {
            screen: OutfitsScreen,
        },
        [ROUTES.PLACES.ROUTE]: {
            screen: PlacesScreen,
        },
        [ROUTES.REVIEW_WATCH.ROUTE]: {
            screen: ReviewWatchScreen,
        },
        [ROUTES.SINGLE_COLLECTION.ROUTE]: {
            screen: SingleCollectionScreen,
        },
        [ROUTES.SINGLE_OUTFIT.ROUTE]: {
            screen: SingleOutfitScreen,
        },
        [ROUTES.SINGLE_PLACE.ROUTE]: {
            screen: SinglePlaceScreen,
        }
    },
);

export default ListStack;
