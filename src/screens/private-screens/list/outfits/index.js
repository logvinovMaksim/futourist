
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class OutfitsScreen extends PureComponent {
    outfitsRequest = () => this.props.outfitsRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text variant="h6">Welcome to React Native advanced template!</Text>
                    <Text variant="h5">You currently at Outfits Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.outfitsRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

OutfitsScreen.displayName = 'OutfitsScreen';

OutfitsScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    outfitsRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.outfits }),
    dispatch => ({
        outfitsRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(OutfitsScreen);
