
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class PlacesScreen extends PureComponent {
    placesRequest = () => this.props.placesRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Places Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.placesRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

PlacesScreen.displayName = 'PlacesScreen';

PlacesScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    placesRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.places }),
    dispatch => ({
        placesRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(PlacesScreen);
