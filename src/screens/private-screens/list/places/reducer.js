
// local dependencies
import TYPE from './types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case TYPE.START:
            state = { expectAnswer: true };
            break;
        case TYPE.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            break;
    }

    return state;
}
