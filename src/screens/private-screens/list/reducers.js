
// local dependencies
import collections from './collections/reducer';
import listHolder from './list-holder/reducer';
import outfits from './outfits/reducer';
import places from './places/reducer';
import reviewWatch from './review-watch/reducer';
import singleCollection from './single-collection/reducer';
import singleOutfit from './single-outfit/reducer';
import singlePlace from './single-place/reducer';

/**
 * connect all list private reducers
 *
 */
export default {
    collections,
    listHolder,
    outfits,
    places,
    reviewWatch,
    singleCollection,
    singleOutfit,
    singlePlace,
};
