
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class ReviewWatchScreen extends PureComponent {
    reviewWatchRequest = () => this.props.reviewWatchRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Review Watch Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.reviewWatchRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

ReviewWatchScreen.displayName = 'ReviewWatchScreen';

ReviewWatchScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    reviewWatchRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.reviewWatch }),
    dispatch => ({
        reviewWatchRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(ReviewWatchScreen);
