
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import collections from './collections/saga';
import listHolder from './list-holder/saga';
import outfits from './outfits/saga';
import places from './places/saga';
import reviewWatch from './review-watch/saga';
import singleCollection from './single-collection/saga';
import singleOutfit from './single-outfit/saga';
import singlePlace from './single-place/saga';

/**
 * connect all profile private sagas
 *
 */
export default function* () {
    yield fork(collections);
    yield fork(listHolder);
    yield fork(outfits);
    yield fork(places);
    yield fork(reviewWatch);
    yield fork(singleCollection);
    yield fork(singleOutfit);
    yield fork(singlePlace);
}
