
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class SingleCollectionScreen extends PureComponent {
    singleCollectionRequest = () => this.props.singleCollectionRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Single Collection Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.singleCollectionRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

SingleCollectionScreen.displayName = 'SingleCollectionScreen';

SingleCollectionScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    singleCollectionRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.singleCollection }),
    dispatch => ({
        singleCollectionRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(SingleCollectionScreen);
