// local dependencies

export default (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@single-collection/');
