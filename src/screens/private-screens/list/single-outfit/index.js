
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import TYPE from './types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class SingleOutfitScreen extends PureComponent {
    singleOutfitRequest = () => this.props.singleOutfitRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Single Outfit Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.singleOutfitRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

SingleOutfitScreen.displayName = 'SingleOutfitScreen';

SingleOutfitScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    singleOutfitRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.singleOutfit }),
    dispatch => ({
        singleOutfitRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(SingleOutfitScreen);
