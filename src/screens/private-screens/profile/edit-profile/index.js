
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import { EDIT_PROFILE } from '../types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class EditProfileScreen extends PureComponent {
    editProfileRequest = () => this.props.editProfileRequest();

    goToSettingsScreen = () => this.props.navigation.navigate('SettingsScreen');

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Edit Profile Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.editProfileRequest} />
                    </View>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Settings Screen" onPress={this.goToSettingsScreen} />
                    </View>
                </View>
            </Screen>
        );
    }
}

EditProfileScreen.displayName = 'EditProfileScreen';

EditProfileScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    editProfileRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.editProfile }),
    dispatch => ({
        editProfileRequest: () => dispatch({ type: EDIT_PROFILE.REQUEST }),
    })
)(EditProfileScreen);
