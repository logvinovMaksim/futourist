
// local dependencies
import { EDIT_PROFILE } from '../types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case EDIT_PROFILE.START:
            state = { expectAnswer: true };
            break;
        case EDIT_PROFILE.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
