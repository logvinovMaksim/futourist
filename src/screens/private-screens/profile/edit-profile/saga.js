
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { EDIT_PROFILE } from '../types';

function* editProfileFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: EDIT_PROFILE.SUCCESS });
    } catch (error) {
        yield put({ type: EDIT_PROFILE.ERROR, errorMessage: error.message });
    }
    yield put({ type: EDIT_PROFILE.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(EDIT_PROFILE.REQUEST, editProfileFlow);
}
