
// outsource dependencies
import React from 'react';
import { createStackNavigator } from 'react-navigation';

// local dependencies
import AppHeader from '../../../components/header/app-header';

import * as ROUTES from '../../../constants/routes';
import SettingsScreen from './settings';
import TermsOfServiceScreen from './terms-of-service';
import EditProfileScreen from './edit-profile';
import ProfileScreen from './profile';

const ProfileStack = createStackNavigator(
    {
        [ROUTES.PROFILE.ROUTE]: {
            screen: ProfileScreen,
        },
        [ROUTES.EDIT_PROFILE.ROUTE]: {
            screen: EditProfileScreen,
        },
        [ROUTES.SETTINGS.ROUTE]: {
            screen: SettingsScreen,
        },
        [ROUTES.TERMS_OF_SERVICE.ROUTE]: {
            screen: TermsOfServiceScreen,
        }
    },
    {
        headerLayoutPreset: 'center',
        defaultNavigationOptions: ({ navigation }) => ({
            headerLayoutPreset: 'center',
            title: 'Profile',
            headerLeft: <AppHeader navigation={navigation} arrow />,
        })
    }
);

export default ProfileStack;
