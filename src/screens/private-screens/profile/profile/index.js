
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Button from '../../../../components/button/button';
import { PROFILE } from '../types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class ProfileScreen extends PureComponent {
    profileRequest = () => this.props.profileRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Button label="Log out" onPress={() => this.props.navigation.navigate('Public')} />
                </View>
            </Screen>
        );
    }
}

ProfileScreen.displayName = 'ProfileScreen';

ProfileScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    profileRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.editProfile }),
    dispatch => ({
        profileRequest: () => dispatch({ type: PROFILE.REQUEST }),
    })
)(ProfileScreen);
