
// local dependencies
import { PROFILE } from '../types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case PROFILE.START:
            state = { expectAnswer: true };
            break;
        case PROFILE.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
