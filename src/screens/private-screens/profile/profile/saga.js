
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { PROFILE } from '../types';

function* profileFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: PROFILE.SUCCESS });
    } catch (error) {
        yield put({ type: PROFILE.ERROR, errorMessage: error.message });
    }
    yield put({ type: PROFILE.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(PROFILE.REQUEST, profileFlow);
}
