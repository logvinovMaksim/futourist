
// local dependencies
import profile from './profile/reducer';
import editProfile from './edit-profile/reducer';
import settings from './settings/reducer';
import termsOfService from './terms-of-service/reducer';
/**
 * connect all profile private reducers
 *
 * @public
 */

export default {
    profile,
    editProfile,
    settings,
    termsOfService,
};
