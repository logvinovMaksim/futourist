
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import profile from './profile/saga';
import editProfile from './edit-profile/saga';
import settings from './settings/saga';
import termsOfService from './terms-of-service/saga';

/**
 * connect all profile private sagas
 *
 * @public
 */
export default function* () {
    yield fork(profile);
    yield fork(editProfile);
    yield fork(settings);
    yield fork(termsOfService);
}
