
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import { SETTINGS } from '../types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class SettingsScreen extends PureComponent {
    settingsRequest = () => this.props.settingsRequest();

    goToCollectionsScreen= () => this.props.navigation.navigate('CollectionsScreen');

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Settings Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.settingsRequest} />
                    </View>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Collections Screen" onPress={this.goToCollectionsScreen} />
                    </View>
                </View>
            </Screen>
        );
    }
}

SettingsScreen.displayName = 'SettingsScreen';

SettingsScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    settingsRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.settings }),
    dispatch => ({
        settingsRequest: () => dispatch({ type: SETTINGS.REQUEST }),
    })
)(SettingsScreen);
