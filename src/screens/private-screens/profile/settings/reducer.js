
// local dependencies
import { SETTINGS } from '../types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case SETTINGS.START:
            state = { expectAnswer: true };
            break;
        case SETTINGS.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
