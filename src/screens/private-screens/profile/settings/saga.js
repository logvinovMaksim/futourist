
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { SETTINGS } from '../types';

function* settingsFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: SETTINGS.SUCCESS });
    } catch (error) {
        yield put({ type: SETTINGS.ERROR, errorMessage: error.message });
    }
    yield put({ type: SETTINGS.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(SETTINGS.REQUEST, settingsFlow);
}
