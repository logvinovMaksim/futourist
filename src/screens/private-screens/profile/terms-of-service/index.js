
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Button } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Screen from '../../../../components/screen';
import Text from '../../../../components/text';
import { TERMS_OF_SERVICE } from '../types';
import { APP_OFFSETS } from '../../../../constants/app.theme';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class TermsOfServiceScreen extends PureComponent {
    termsOfServiceRequest = () => this.props.termsOfServiceRequest();

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Text center variant="boldTitle">Welcome to React Native advanced template!</Text>
                    <Text center variant="subtitle">You currently at Terms Of Service Screen</Text>
                    <View style={styles.supportRequestBtn}>
                        <Button title="Start request" onPress={this.termsOfServiceRequest} />
                    </View>
                </View>
            </Screen>
        );
    }
}

TermsOfServiceScreen.displayName = 'TermsOfServiceScreen';

TermsOfServiceScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    termsOfServiceRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.termsOfService }),
    dispatch => ({
        termsOfServiceRequest: () => dispatch({ type: TERMS_OF_SERVICE.REQUEST }),
    })
)(TermsOfServiceScreen);
