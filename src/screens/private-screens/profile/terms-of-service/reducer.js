
// local dependencies
import { TERMS_OF_SERVICE } from '../types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case TERMS_OF_SERVICE.START:
            state = { expectAnswer: true };
            break;
        case TERMS_OF_SERVICE.SUCCESS:
            state = { expectAnswer: false };
            break;
        default:
            return state;
    }
}
