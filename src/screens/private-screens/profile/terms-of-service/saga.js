
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import { TERMS_OF_SERVICE } from '../types';

function* termsOfServiceFlow (action) {
    const { type, ...options } = action;
    try {
        yield put({ type: TERMS_OF_SERVICE.SUCCESS });
    } catch (error) {
        yield put({ type: TERMS_OF_SERVICE.ERROR, errorMessage: error.message });
    }
    yield put({ type: TERMS_OF_SERVICE.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(TERMS_OF_SERVICE.REQUEST, termsOfServiceFlow);
}
