// local dependencies

export const EDIT_PROFILE = (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@edit-profile/');

export const PROFILE = (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@profile/');

export const SETTINGS = (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@settings/');

export const TERMS_OF_SERVICE = (prefix => {
    return {
        // simple action
        REQUEST: `${prefix}REQUEST`,
        START: `${prefix}START`,
        SUCCESS: `${prefix}SUCCESS`,
        FINISH: `${prefix}FINISH`,
        ERROR: `${prefix}ERROR`,
        CLEAR: `${prefix}CLEAR`,
    };
})('@terms-of-service/');
