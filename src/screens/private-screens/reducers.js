
// local dependencies
import activity from './activity/reducer';
import home from './home/reducer';
import search from './search/reducer';
import addReviewReducers from './add-review/reducers';
import listReducers from './list/reducers';
import profileReducers from './profile/reducers';
/**
 * connect all private reducers
 *
 */

export default {
    activity,
    home,
    search,
    ...listReducers,
    ...profileReducers,
    ...addReviewReducers,
};
