
// outsource dependencies
import React from 'react';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

// local dependencies
import { APP_COLORS } from '../../constants/app.theme';
import * as ROUTES from '../../constants/routes';
// screens stacks
import SearchStack from './search/navigation-config';
import HomeStack from './home/navigation-config';
import ReviewStack from './add-review/navigation-config';
import ActivityStack from './activity/navigation-config';
import ProfileStack from './profile/navigation-config';
import ListStack from './list/navigation-config';

const TabNavigator = createBottomTabNavigator(
    {
        [ROUTES.HOME.ROUTE]: HomeStack,
        [ROUTES.SEARCH.ROUTE]: SearchStack,
        [ROUTES.ACTIVITY.ROUTE]: ActivityStack,
        Review: ReviewStack,
        Profile: ProfileStack,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            // eslint-disable-next-line
            tabBarIcon: ({ focused, horizontal, tintColor = APP_COLORS.THEME_DISABLED_COLOR }) => {
                const { routeName } = navigation.state;
                let iconName = 'tree';
                if (routeName === ROUTES.HOME.ROUTE) { iconName = 'home'; }
                if (routeName === ROUTES.SEARCH.ROUTE) { iconName = 'search'; }
                if (routeName === ROUTES.ACTIVITY.ROUTE) { iconName = 'plus-circle'; }
                if (routeName === 'Review') { iconName = 'heart-o'; }
                if (routeName === 'Profile') { iconName = 'user-secret'; }
                return (<Icon name={iconName} size={20} color={tintColor} />);
            },
        }),
        backBehavior: 'history',
        initialRouteName: ROUTES.HOME.ROUTE,
        tabBarOptions: {
            activeTintColor: APP_COLORS.TURQUOISE,
            inactiveTintColor: APP_COLORS.THEME_DISABLED_COLOR,
            activeBackgroundColor: APP_COLORS.WHITE,
            inactiveBackgroundColor: APP_COLORS.WHITE,
        },
    });

const PrivateScreens = createStackNavigator(
    {
        BottomTabsStack: TabNavigator,
        ListStack: ListStack,
    },
    {
        headerMode: 'none',
        header: null,
    }
);

export default PrivateScreens;
