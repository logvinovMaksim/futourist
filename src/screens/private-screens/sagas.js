
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import activity from './activity/saga';
import home from './home/saga';
import search from './search/saga';
import addReviewWatcher from './add-review/sagas';
import listWatcher from './list/sagas';
import profileWatcher from './profile/sagas';

/**
 * connect all private sagas
 *
 * @public
 */
export default function* () {
    yield fork(activity);
    yield fork(home);
    yield fork(search);
    yield fork(addReviewWatcher);
    yield fork(listWatcher);
    yield fork(profileWatcher);
}
