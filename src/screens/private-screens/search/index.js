
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

// local dependencies
import Input, { INPUT_TYPE } from '../../../components/input/input';
import Screen from '../../../components/screen';
import TYPE from './types';
import { APP_OFFSETS, APP_COLORS } from '../../../constants/app.theme';
import Button, { BUTTON_TYPE } from '../../../components/button/button';

const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP,
    },
});

class SearchScreen extends PureComponent {

    render () {
        return (
            <Screen>
                <View style={styles.content}>
                    <Input
                        ref={ref => this._search = ref}
                        type={INPUT_TYPE.SEARCH}
                        placeholder="What you`re exploring?"
                    />
                    <Input
                        ref={ref => this._search = ref}
                        type={INPUT_TYPE.SEARCH}
                        placeholder="Near me"
                    />
                    <View style={{ flexDirection: 'row' }}>
                        <Button
                            icon={{ name: 'android', color: APP_COLORS.MAIN }}
                            type={BUTTON_TYPE.CATEGORY}
                            label="Category"
                            onPress={() => {}}
                            buttonStyles={{ marginRight: 8 }}
                        />
                        <Button
                            icon={{ name: 'android', color: APP_COLORS.MAIN }}
                            type={BUTTON_TYPE.CATEGORY}
                            label="Category"
                            onPress={() => {}}
                        />
                    </View>
                </View>
            </Screen>
        );
    }
}

SearchScreen.displayName = 'SearchScreen';

SearchScreen.propTypes = {
    // navigation: PropTypes.object.isRequired,
    // searchRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.search }),
    dispatch => ({
        searchRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(SearchScreen);
