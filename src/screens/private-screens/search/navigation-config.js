
// outsource dependencies
import React from 'react';
import { createStackNavigator } from 'react-navigation';

// local dependencies
import SearchScreen from './index';
import AppHeader from '../../../components/header/app-header';

const SearchStack = createStackNavigator(
    {
        Search: {
            screen: SearchScreen,
            navigationOptions: ({ navigation }) => ({
                title: 'Search for a place',
                headerLeft: <AppHeader navigation={navigation} arrow />,
            }),
        }
    },
    {
        headerLayoutPreset: 'center',
        defaultNavigationOptions: {
            headerLayoutPreset: 'center',
        }
    }
);

export default SearchStack;
