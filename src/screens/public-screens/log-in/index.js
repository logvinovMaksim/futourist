
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image } from 'react-native';
import { connect } from 'react-redux';
import {
    LoginManager,
    AccessToken,
    GraphRequest,
    GraphRequestManager,
} from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';

// local dependencies
import Screen from '../../../components/screen';
import Text from '../../../components/text';
import TYPE from './types';
import { APP_COLORS, APP_OFFSETS } from '../../../constants/app.theme';
import Button, { BUTTON_TYPE } from '../../../components/button/button';
import { HOME } from '../../../constants/routes';
import Input, { INPUT_TYPE } from '../../../components/input/input';
// import { HOME } from '../../../constants/routes';


const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
    },
    title: {
        marginTop: APP_OFFSETS.TOP,
    },
    choose: {
        marginTop: APP_OFFSETS.TOP / 2,
    },
    supportRequestBtn: {
        marginTop: APP_OFFSETS.TOP * 2,
        flexDirection: 'row',
    },
    socialButtons: {
        marginTop: APP_OFFSETS.TOP * 2,
        flexDirection: 'row',
    },
    facebook: {
        backgroundColor: APP_COLORS.FACEBOOK,
        marginRight: APP_OFFSETS.RIGHT,
    },
    google: {
        backgroundColor: APP_COLORS.WHITE,
    },
    googleLogo: {
        width: 20,
        height: 20,
    },
    logIn: {
        marginTop: APP_OFFSETS.TOP,
        width: '100%',
    },
});

class LogInScreen extends PureComponent {

    componentDidMount () {
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Replace with your webClientId generated from Firebase console
            webClientId:
                '734816963355-i9ug0b4an48p75og78csb58e6ob4p8rq.apps.googleusercontent.com',
        });
    }

    logInRequest = () => this.props.logInRequest();

    facebook = async () => {
        LoginManager.logInWithPermissions(['public_profile', 'email', 'user_friends']).then(
            function (result) {
                if (result.isCancelled) {
                    console.info('Login cancelled');
                } else {
                    console.info(`Login success with permissions: ${result.grantedPermissions.toString()}`);
                }
            },
            function (error) {
                console.info(`Login fail with error: ${error}`);
            }
        );
        this.props.navigation.navigate(HOME.LINK());
    };

    google = async () => {
        //Prompts a modal to let the user sign in into your application.
        try {
            await GoogleSignin.hasPlayServices({
                //Check if device has Google Play Services installed.
                //Always resolves to true on iOS.
                showPlayServicesUpdateDialog: true,
            });
            const userInfo = await GoogleSignin.signIn();
            console.info('User Info --> ', userInfo);
            // this.setState({ userInfo: userInfo });
        } catch (error) {
            console.info('Message', error.message, error);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.info('User Cancelled the Login Flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.info('Signing In');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.info('Play Services Not Available or Outdated');
            } else {
                console.info('Some Other Error Happened');
            }
        }
        this.props.navigation.navigate(HOME.LINK());
    };

    login = () => this.props.navigation.navigate(HOME.LINK());

    render () {
        return (
            <Screen initialized>
                <View style={styles.content}>
                    <View style={styles.title}>
                        <Text variant="h2">Welcome back, Futourist!</Text>
                    </View>
                    <View style={styles.socialButtons}>
                        <Button
                            type={BUTTON_TYPE.SOCIAL}
                            icon={{ name: 'facebook-square' }}
                            buttonStyles={styles.facebook}
                            labelColor={APP_COLORS.WHITE}
                            label="Facebook"
                            onPress={this.facebook}
                        />
                        <Button
                            type={BUTTON_TYPE.SOCIAL}
                            buttonStyles={styles.google}
                            labelColor={APP_COLORS.BLACK}
                            label="Google"
                            onPress={this.google}
                        >
                            <Image style={styles.googleLogo} source={require('../../../../assets/google-logo.png')} />
                        </Button>
                    </View>
                    <View style={styles.choose}>
                        <Text variant="h2">or</Text>
                    </View>
                    <Input placeholder="Enter username" />
                    <Input
                        ref={ref => this._password = ref}
                        type={INPUT_TYPE.PASSWORD}
                        placeholder="Enter password"
                    />
                    <View style={styles.logIn}>
                        <Button label="LOG IN" onPress={this.login} />
                    </View>
                </View>
            </Screen>
        );
    }
}

LogInScreen.displayName = 'LogInScreen';

LogInScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    logInRequest: PropTypes.func.isRequired,
};

export default connect(
    state => ({ ...state.logIn }),
    dispatch => ({
        logInRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(LogInScreen);
