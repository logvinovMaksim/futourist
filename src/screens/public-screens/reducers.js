
// local dependencies
import start from './start/reducer';
import signUp from './sign-up/reducer';
import logIn from './log-in/reducer';

/**
 * connect all public reducers
 *
 * @public
 */
export default {
    start,
    signUp,
    logIn
};
