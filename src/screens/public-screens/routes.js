
// outsource dependencies
import React from 'react';
import { createStackNavigator } from 'react-navigation';

// local dependencies
import AppHeader from '../../components/header/app-header';
import * as ROUTES from '../../constants/routes';
import LogInScreen from './log-in';
import SignUpScreen from './sign-up';
import StartScreen from './start';

const PublicScreens = createStackNavigator(
    {
        [ROUTES.START.ROUTE]: {
            screen: StartScreen,
            navigationOptions: {
                header: null,
            }
        },
        [ROUTES.SIGN_UP.ROUTE]: {
            screen: SignUpScreen,
            navigationOptions: ({ navigation }) => ({
                headerLeft: <AppHeader navigation={navigation} />,
                headerStyle: {
                    shadowColor: 'transparent', // disable header shadow on IOS
                    elevation: 0, // disable header shadow on Android
                },
            }),
        },
        [ROUTES.LOG_IN.ROUTE]: {
            screen: LogInScreen,
            navigationOptions: ({ navigation }) => ({
                headerLeft: <AppHeader navigation={navigation} />,
                headerStyle: {
                    shadowColor: 'transparent', // disable header shadow on IOS
                    elevation: 0, // disable header shadow on Android
                },
            }),
        }
    },
    {
        initialRouteName: ROUTES.START.ROUTE,
    }
);

export default PublicScreens;
