
// outsource dependencies
import { fork } from 'redux-saga/effects';

// local dependencies
import start from './start/saga';
import signUp from './sign-up/saga';
import logIn from './log-in/saga';

/**
 * connect all public sagas
 *
 * @public
 */
export default function* () {
    yield fork(signUp);
    yield fork(start);
    yield fork(logIn);
}
