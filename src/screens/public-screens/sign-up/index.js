
// outsource dependencies
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Switch, ScrollView, Image } from 'react-native';
import {
    LoginManager,
    AccessToken,
    GraphRequest,
    GraphRequestManager,
} from 'react-native-fbsdk';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';

// local dependencies
import Screen from '../../../components/screen';
import Text from '../../../components/text';
import TYPE from './types';
import { APP_COLORS, APP_OFFSETS } from '../../../constants/app.theme';
import Button, { BUTTON_TYPE } from '../../../components/button/button';
import Input, { INPUT_TYPE } from '../../../components/input/input';
import { HOME } from '../../../constants/routes';

const styles = StyleSheet.create({
    content: {
        alignItems: 'center',
    },
    title: {
        marginTop: APP_OFFSETS.TOP,
    },
    choose: {
        marginTop: APP_OFFSETS.TOP / 2,
    },
    socialButtons: {
        marginTop: APP_OFFSETS.TOP * 2,
        flexDirection: 'row',
    },
    facebook: {
        backgroundColor: APP_COLORS.FACEBOOK,
        marginRight: APP_OFFSETS.RIGHT,
    },
    google: {
        backgroundColor: APP_COLORS.WHITE,
    },
    terms: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: APP_OFFSETS.BOTTOM,
    },
    subscribe: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    googleLogo: {
        width: 20,
        height: 20,
    },
    signUp: {
        marginTop: APP_OFFSETS.TOP,
        width: '100%',
    },
});

class SignUpScreen extends PureComponent {
    constructor (props) {
        super(props);
        this.state = {
            isTermsAgreed: true,
            isSubscribeAgreed: true,
            isUsernameFocused: false,
            isEmailFocused: false,
            isPasswordFocused: false,

        };
    }

    componentDidMount () {
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Replace with your webClientId generated from Firebase console
            webClientId:
                '734816963355-i9ug0b4an48p75og78csb58e6ob4p8rq.apps.googleusercontent.com',
        });
        // firebase.initializeApp({
        //     apiKey: 'AIzaSyCboYaidigK7z3eu5Ho2i7EDld08yFqAFg',
        //     authDomain: 'rntemplate-277e5.web.app',
        //     databaseURL: 'https://rntemplate-277e5.firebaseio.com',
        //     storageBucket: 'gs://rntemplate-277e5.appspot.com/'
        // });
        // works
        // firebase.database().ref('users/001').set('dfdfdg').then(() => {});
        // firebase.storage().ref();
        // console.log('firebase storage', test);
        console.info('firebase.auth()', firebase.auth());
    }

    // goToHome = () => this.props.navigation.navigate(HOME.LINK());

    google = async () => {
        //Prompts a modal to let the user sign in into your application.
        const isUserSignedIn = await GoogleSignin.isSignedIn();
        console.log('isUserSignedIn', isUserSignedIn);
        try {
            await GoogleSignin.hasPlayServices({
                //Check if device has Google Play Services installed.
                //Always resolves to true on iOS.
                showPlayServicesUpdateDialog: true,
            });
            const userInfo = await GoogleSignin.signIn();
            console.info('User Info --> ', userInfo);
            // this.setState({ userInfo: userInfo });
        } catch (error) {
            console.info('Message', error.message, error);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.info('User Cancelled the Login Flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.info('Signing In');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.info('Play Services Not Available or Outdated');
            } else {
                console.info('Some Other Error Happened');
            }
        }
        this.props.navigation.navigate(HOME.LINK());
    };
    // _getCurrentUser = async () => {
    //     //May be called eg. in the componentDidMount of your main component.
    //     //This method returns the current user
    //     //if they already signed in and null otherwise.
    //     try {
    //         const userInfo = await GoogleSignin.signInSilently();
    //         this.setState({ userInfo });
    //     } catch (error) {
    //         console.error(error);
    //     }
    // };
    // _signOut = async () => {
    //     //Remove user session from the device.
    //     try {
    //         await GoogleSignin.revokeAccess();
    //         await GoogleSignin.signOut();
    //         this.setState({ user: null }); // Remove the user from your app's state as well
    //     } catch (error) {
    //         console.error(error);
    //     }
    // };
    // _revokeAccess = async () => {
    //     //Remove your application from the user authorized applications.
    //     try {
    //         await GoogleSignin.revokeAccess();
    //         console.log('deleted');
    //     } catch (error) {
    //         console.error(error);
    //     }
    // };

    facebook = async () => {
        // native_only config will fail in the case that the user has
        // not installed in his device the Facebook app. In this case we
        // need to go for webview.
        // let result;
        // try {
        //     LoginManager.setLoginBehavior('NATIVE_ONLY');
        //     result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        // } catch (nativeError) {
        //     try {
        //         LoginManager.setLoginBehavior('WEB_ONLY');
        //         result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
        //     } catch (webError) {
        //         // show error message to the user if none of the FB screens
        //         // did not open
        //     }
        // }
        // // handle the case that users clicks cancel button in Login view
        // if (result.isCancelled) {
        //     // this.setState({
        //     //     showLoadingModal: false,
        //     //     notificationMessage: I18n.t('welcome.FACEBOOK_CANCEL_LOGIN')
        //     // });
        // } else {
        //     // Create a graph request asking for user information
        //     this.FBGraphRequest('id, email, picture.type(large)', this.FBLoginCallback);
        // }

        await LoginManager.logInWithPermissions(['public_profile', 'email']).then(
            (result) => {
                if (result.isCancelled) {
                    console.info('Login cancelled');
                } else {
                    console.info(`Login success with permissions: ${result.grantedPermissions.toString()}`);
                    this.FBGraphRequest('id, email, picture.type(large)', this.FBLoginCallback);
                }
            },
            function (error) {
                console.info(`Login fail with error: ${error}`);
            }
        );
        // this.props.navigation.navigate(HOME.LINK());
    };


    FBGraphRequest = async (fields, callback) => {
        const accessData = await AccessToken.getCurrentAccessToken();
        // Create a graph request asking for user information
        const infoRequest = new GraphRequest('/me', {
            accessToken: accessData.accessToken,
            parameters: {
                fields: {
                    string: fields
                }
            }
        }, callback.bind(this));
        // Execute the graph request created above
        new GraphRequestManager().addRequest(infoRequest).start();
        console.info('start request fb');

    };

    FBLoginCallback = async (error, result) => {
        if (error) {
            // this.setState({ showLoadingModal: false });
            console.info('fb error', error);
        } else {
            // Retrieve and save user details in state. In our case with
            // Redux and custom action saveUser
            console.info('fb result', result);
            // this.setState({
            //     id: result.id,
            //     email: result.email,
            //     image: result.picture.data.url
            // });
        }
    };

    // set behavior on keyboard show event
    handleKeyboard = () => {
        const { isPasswordFocused, isUsernameFocused, isEmailFocused } = this.state;
        if (isUsernameFocused) {
            this._scroll.scrollTo({ x: 0, y: 0, animated: true });
        }

        if (isEmailFocused) {
            this._scroll.scrollTo({ x: 0, y: 120, animated: true });
        }

        if (isPasswordFocused) {
            this._scroll.scrollToEnd({ animated: true });
        }

    };

    // need to duplicate some inputs behavior that were previously justified in keyboard event
    // for case when keyboard were not hidden
    handleUsernameFocus = () => {
        this.setState({ isUsernameFocused: true });
        this._scroll.scrollTo({ x: 0, y: 0, animated: true });
    };

    handleUsernameBlur = () => {
        this.setState({ isUsernameFocused: false });
    };

    handleEmailFocus = () => {
        this.setState({ isEmailFocused: true });
        this._scroll.scrollTo({ x: 0, y: 120, animated: true });
    };

    handleEmailBlur = () => {
        this.setState({ isEmailFocused: false });
    };

    handlePasswordFocus = () => {
        this.setState({ isPasswordFocused: true });
        this._scroll.scrollToEnd({ animated: true });
    };

    handlePasswordBlur = () => {
        this.setState({ isPasswordFocused: false });
    };

    subscribe = () => {
        const { isSubscribeAgreed } = this.state;
        this.setState({ isSubscribeAgreed: !isSubscribeAgreed });
    };

    terms = () => {
        const { isTermsAgreed } = this.state;
        this.setState({ isTermsAgreed: !isTermsAgreed });
    };

    scrollRef = ref => { this._scroll = ref; };

    render () {
        const { isTermsAgreed, isSubscribeAgreed } = this.state;
        return (
            <Screen initialized keyboard={this.handleKeyboard}>
                <ScrollView
                    ref={this.scrollRef}
                    keyboardDismissMode="on-drag"
                    keyboardShouldPersistTaps="handled"
                    contentContainerStyle={styles.content}
                >
                    <View style={styles.title}>
                        <Text variant="h2">Hi, Futourist! Sign up:</Text>
                    </View>
                    <View style={styles.socialButtons}>
                        <Button
                            type={BUTTON_TYPE.SOCIAL}
                            icon={{ name: 'facebook-square' }}
                            buttonStyles={styles.facebook}
                            labelColor={APP_COLORS.WHITE}
                            label="Facebook"
                            onPress={this.facebook}
                        />
                        <Button
                            type={BUTTON_TYPE.SOCIAL}
                            buttonStyles={styles.google}
                            labelColor={APP_COLORS.BLACK}
                            label="Google"
                            onPress={this.google}
                        >
                            <Image style={styles.googleLogo} source={require('../../../../assets/google-logo.png')} />
                        </Button>
                    </View>
                    <View style={styles.choose}>
                        <Text variant="h2">or</Text>
                        {/*<LoginButton*/}
                        {/*onLoginFinished={*/}
                        {/*(error, result) => {*/}
                        {/*if (error) {*/}
                        {/*console.info(`login has error:  ${result.error}`);*/}
                        {/*} else if (result.isCancelled) {*/}
                        {/*console.info('login is cancelled.');*/}
                        {/*} else {*/}
                        {/*AccessToken.getCurrentAccessToken().then(*/}
                        {/*(data) => {*/}
                        {/*console.info(data.accessToken.toString());*/}
                        {/*});*/}
                        {/*}*/}
                        {/*}*/}
                        {/*}*/}
                        {/*onLogoutFinished={() => console.info('logout.')}*/}
                        {/*/>*/}
                    </View>
                    <Input
                        placeholder="Enter your username"
                        onBlur={this.handleUsernameBlur}
                        onFocus={this.handleUsernameFocus}
                    />
                    <Input
                        placeholder="Enter your email"
                        onBlur={this.handleEmailBlur}
                        onFocus={this.handleEmailFocus}
                    />
                    <Input
                        ref={ref => this._password = ref}
                        type={INPUT_TYPE.PASSWORD}
                        placeholder="Enter password"
                        onBlur={this.handlePasswordBlur}
                        onFocus={this.handlePasswordFocus}
                    />
                    <View style={styles.terms}>
                        <Text>I agree to Terms & Conditions</Text>
                        <Switch
                            thumbColor={APP_COLORS.WHITE}
                            value={isTermsAgreed}
                            onValueChange={this.terms}
                        />
                    </View>
                    <View style={styles.subscribe}>
                        <Text>Subscribe to our Newsletter</Text>
                        <Switch
                            value={isSubscribeAgreed}
                            thumbColor={APP_COLORS.WHITE}
                            onValueChange={this.subscribe}
                        />
                    </View>
                    <View style={styles.signUp}>
                        <Button label="SIGN UP" onPress={() => this.props.navigation.navigate('HomeScreen')} />
                        <Button label="SIGN UP" onPress={() => LoginManager.logOut()} />
                    </View>
                </ScrollView>
            </Screen>
        );
    }
}

SignUpScreen.propTypes = {
    // signUpRequest: PropTypes.func.isRequired,
    // handleSubmit: PropTypes.func.isRequired,
    // signUpSuccess: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
};

export default connect(
    state => ({ ...state.signUp }),
    dispatch => ({
        signUpRequest: () => dispatch({ type: TYPE.REQUEST }),
        signUpSuccess: (userData = '') => dispatch({ type: TYPE.SUCCESS, userData }),
    })
)(SignUpScreen);
