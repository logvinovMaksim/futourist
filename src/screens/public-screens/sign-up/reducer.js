
// local dependencies
import TYPE from './types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
    userData: null
};

export default function (state = initial, action) {
    const { type } = action;
    switch (type) {
        case TYPE.START:
            return { expectAnswer: true };
        case TYPE.SUCCESS:
            return { expectAnswer: false, userData: action.userData };
        default:
            return state;
    }
}
