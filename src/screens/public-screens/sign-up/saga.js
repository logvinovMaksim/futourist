
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import TYPE from './types';

function* signUpScreenFlow(action) {
    const { type, ...options } = action;
    try {
        yield put({ type: TYPE.SUCCESS });
    } catch (error) {
        yield put({ type: TYPE.ERROR, errorMessage: error.message });
    }
    yield put({ type: TYPE.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(TYPE.REQUEST, signUpScreenFlow);
}
