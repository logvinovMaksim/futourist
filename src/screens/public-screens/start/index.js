
// outsource dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

// local dependencies
import LOCAL_IMAGES from '../../../constants/local-images';
import { HOME, LOG_IN, SIGN_UP } from '../../../constants/routes';
import Screen from '../../../components/screen';
import Text from '../../../components/text';
import Button, { BUTTON_TYPE } from '../../../components/button/button';
import TYPE from './types';
import { APP_COLORS } from '../../../constants/app.theme';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    box: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        marginRight: 8,
    },
    logoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    buttonsContainer: {
        width: '100%',
        marginBottom: -32,
    },
});

class StartScreen extends Component {
    startRequest = () => this.props.startRequest();

    logIn = () => this.props.navigation.navigate(LOG_IN.LINK());

    signUp = () => this.props.navigation.navigate(SIGN_UP.LINK());

    skip = () => this.props.navigation.navigate(HOME.LINK());

    render () {
        const { successMessage } = this.props;
        return (
            <Screen initialized source={LOCAL_IMAGES.START}>
                <View style={styles.container}>
                    <View style={styles.logoContainer}>
                        <Icon style={styles.logo} name="flash" size={40} color={APP_COLORS.WHITE} />
                        <Text variant="h4" color={APP_COLORS.WHITE}>FUTOURIST</Text>
                    </View>


                    {/*<Button*/}
                    {/*icon={{ name: 'android', color: APP_COLORS.MAIN }}*/}
                    {/*type={BUTTON_TYPE.CATEGORY}*/}
                    {/*label="Category"*/}
                    {/*onPress={() => console.log('on Press')}*/}
                    {/*/>*/}
                    <View style={styles.buttonsContainer}>
                        <Button label="SIGN UP" onPress={this.signUp} />
                        <Button type={BUTTON_TYPE.SECONDARY} label="LOG IN" onPress={this.logIn} />
                        <Button type={BUTTON_TYPE.LOW} label="SKIP" onPress={this.skip} />
                    </View>
                </View>
            </Screen>
        );
    }
}

StartScreen.propTypes = {
    navigation: PropTypes.object.isRequired,
    startRequest: PropTypes.func.isRequired,
    successMessage: PropTypes.string.isRequired,
};

export default connect(
    state => ({ ...state.start }),
    dispatch => ({
        startRequest: () => dispatch({ type: TYPE.REQUEST }),
    })
)(StartScreen);
