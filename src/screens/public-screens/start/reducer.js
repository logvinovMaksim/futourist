
// local dependencies
import TYPE from './types';

const initial = {
    expectAnswer: false,
    errorMessage: null,
    successMessage: '',
};

export default function (state = initial, action) {
    const { type, successMessage } = action;
    switch (type) {
        case TYPE.START:
            state = { expectAnswer: true };
            break;
        case TYPE.SUCCESS:
            state = { expectAnswer: false, successMessage };
            break;
        default:
            break;
    }

    return state;
}
