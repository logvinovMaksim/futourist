
// outsource dependencies
import { takeLatest, put } from 'redux-saga/effects';

//local dependencies
import NavigationService from '../../../services/navigation';
import { SIGN_UP } from '../../../constants/routes';
import TYPE from './types';

function* startFlow(action) {
    const { type, ...options } = action;
    try {
        yield put({ type: TYPE.SUCCESS, successMessage: 'Success' });
        yield NavigationService.navigate(SIGN_UP.ROUTE, { someParams: 'some params' });
    } catch (error) {
        yield put({ type: TYPE.ERROR, errorMessage: error.message });
    }
    yield put({ type: TYPE.FINISH });
}

/**
 * common request handler
 *
 * @public
 */
export default function* () {
    yield takeLatest(TYPE.REQUEST, startFlow);
}
