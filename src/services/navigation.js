
// outsource dependencies
import { BackHandler, Platform } from 'react-native';
import { NavigationActions } from 'react-navigation';

class NavigationService {
    static get navigator () {
        // eslint-disable-next-line
        return NavigationService._navigator;
    }

    // Use this only with the top-level (root) navigator of your app
    // see https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
    static setupNavigator (navigator) {
        // eslint-disable-next-line
        NavigationService._navigator = navigator;
    }

    static initialize () {
        if (Platform.OS === 'android') {
            BackHandler.addEventListener('hardwareBackPress', NavigationService.hardwareBackPress);
        }
    }

    static hardwareBackPress () {
        console.info('[NavigationService] hardwareBackPress');
        // when the app is mounted, fire up an event listener for Back Events
        // if the event listener returns false, Back will not occur (note that)
        // after some testing, this seems to be the best way to make
        // back always work and also never close the app
        return true;
    }

    static clear () {
        if (Platform.OS === 'android') {
            // when the app is closed, remove the event listener
            BackHandler.removeEventListener('hardwareBackPress');
        }
    }

    static navigate = (routeName, params = {}) => {
        this.navigator.dispatch(
            NavigationActions.navigate({
                routeName,
                params,
            }),
        );
    };

    static goBack = (key = null) => {
        this.navigator.dispatch(
            NavigationActions.back(key),
        );
    };
}

export default NavigationService;
