
// outsource dependencies
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware, compose } from 'redux';

// local dependencies
import rootReducer from './reducers/index';
import rootWatcher from './sagas/index';

// Composing app with development tools ( debugger )
// Visit https://github.com/zalmoxisus/redux-devtools-extension#12-advanced-store-setup for more home
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Build the middleware to run our Saga
const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootWatcher);

export default store;
